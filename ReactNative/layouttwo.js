import { React, useState } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Iconant from 'react-native-vector-icons/AntDesign';
import Iconentypo from 'react-native-vector-icons/Entypo';
import Iconfontisto from 'react-native-vector-icons/Fontisto';

const MSG = function () {
  return (
    <View style={{ alignItems: "center", marginVertical: "50%" }}>
      <Text>No message yet</Text>
    </View>
  )
};
const Matches = function () {
  return (
    <View style={{ alignItems: "center", marginVertical: "50%" }}>
      <Text>No Matches Yet</Text>
    </View>
  )
};
const Likes = function () {
  return (
    <View style={{ alignItems: "center", marginVertical: "50%" }}>
      <Text>No Likes Yet</Text>
    </View>
  )
};
const Recived = function () {
  return (
    <View style={{ alignItems: "center", marginVertical: "50%" }}>
      <Text>No Recived Yet</Text>
    </View>
  )
};
const Given = function () {
  return (
    <View style={{ alignItems: "center", marginVertical: "50%" }}>
      <Text>No Given Yet</Text>
    </View>
  )
};
const Declined = function () {
  return (
    <View style={{ alignItems: "center", marginVertical: "50%" }}>
      <Text>No Declined Yet</Text>
    </View>
  )
};

const Favorite = function () {
  const [screen, setScreen] = useState("Recived");

  return (<>
    <View style={{ flexDirection: "row" }}>
      <View style={{ flex: 2, paddingLeft: 30, paddingTop: 20 }}>
        <Text onPress={() => { setScreen("Recived") }}>Recived</Text>
        {screen === "Recived" && <View
          style={{
            borderBottomColor: "black",
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderBottomColor: "green",
            borderBottomWidth: 10,

          }}
        />}
      </View>
      <View style={{ flex: 2, paddingTop: 20 }}>
        <Text onPress={() => { setScreen("Given") }}>Given</Text>
        {screen === "Given" && <View
          style={{
            borderBottomColor: "black",
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderBottomColor: "green",
            borderBottomWidth: 10
          }}
        />}
      </View>
      <View style={{ flex: 2, paddingTop: 20 }}>
        <Text onPress={() => { setScreen("Declined") }}>Declined</Text>
        {screen === "Declined" && <View
          style={{
            borderBottomColor: "black",
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderBottomColor: "green",
            borderBottomWidth: 10
          }}
        />}
      </View>
    </View>
    {screen === "Recived" && <Recived />}
    {screen === "Given" && <Given />}
    {screen === "Declined" && <Declined />}
  </>)
};

const App = function () {
  const [screen, setScreen] = useState("MSG");

  return (
    <View>
      <View style={{ flexDirection: "row", backgroundColor: "black" }}>
        <View style={{ flex: 2 }}>
          <Iconant name="menuunfold" size={20} color="black" style={{ color: "white", padding: 20 }} />
        </View>
        <View>
          <Icon name="filter" size={20} color="black" style={{ color: "white", padding: 20 }} />
        </View>
        <View>
          <Iconant name="message1" size={20} color="black" style={{ color: "white", padding: 20 }} />
        </View>
      </View>
      <View style={{ flexDirection: "row", display: "flex", justifyContent: "space-evenly" }}>
        <Text onPress={() => { setScreen("MSG") }}>
          <View style={{ flex: 2 }}>
            <Iconentypo name="message" size={30} style={{ color: "black", padding: 20 }} />
            <Text style={{ paddingLeft: 10 }}>Message</Text>
            {screen === "MSG" && <View
              style={{
                borderBottomColor: "black",
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: "green",
                borderBottomWidth: 10
              }}
            />}
          </View>
        </Text>
        <Text onPress={() => { setScreen("Matches") }}>
          <View >
            <Iconant name="addusergroup" size={30} style={{ color: "black", padding: 20 }} />
            <Text style={{ paddingLeft: 10 }}>Matches</Text>
            {screen === "Matches" && <View
              style={{
                borderBottomColor: "black",
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: "green",
                borderBottomWidth: 10
              }}
            />}
          </View>
        </Text >
        <Text onPress={() => { setScreen("Likes") }}>
          <View >
            <Iconant name="hearto" size={30} style={{ color: "black", padding: 20 }} />
            <Text style={{ paddingLeft: 15 }}>Likes</Text>
            {screen === "Likes" && <View
              style={{
                borderBottomColor: "black",
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: "green",
                borderBottomWidth: 10
              }}
            />}
          </View>
        </Text>
        <Text onPress={() => { setScreen("Favorite") }}>
          <View>
            <Iconfontisto name="favorite" size={30} style={{ color: "black", padding: 20 }} />
            <Text style={{ paddingLeft: 10 }}>Favorite</Text>
            {screen === "Favorite" && <View
              style={{
                borderBottomColor: "black",
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: "green",
                borderBottomWidth: 10
              }}
            />}
          </View>
        </Text>
      </View>
      {screen === "MSG" && <MSG />}
      {screen === "Matches" && <Matches />}
      {screen === "Likes" && <Likes />}
      {screen === "Favorite" && <Favorite />}
    </View>
  );
}

export default App;