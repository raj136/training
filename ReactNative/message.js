import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Iconant from 'react-native-vector-icons/AntDesign';
import Iconentypo from 'react-native-vector-icons/Entypo';
import Iconfontisto from 'react-native-vector-icons/Fontisto';




export default function App() {



  return (
   <View>
    <View style={{flexDirection:"row",backgroundColor:"black"}}> 

 <View style={{flex:2}}>
    <Iconant name="menuunfold" size={20} color="black"  style={{color:"white",padding:20}}/>
 </View>

 <View>
    <Icon name="filter" size={20} color="black"  style={{color:"white",padding:20}}/>
</View>

 <View>
      <Iconant name="message1" size={20} color="black"  style={{color:"white",padding:20}}/>

 </View>

    </View>

    <View style={{flexDirection:"row",display:"flex",justifyContent:"space-evenly"}}>
      <TouchableHighlight > 
 <View >
      <Iconentypo name="message" size={30}  style={{color:"black",padding:20}}/>
      <Text style={{paddingLeft:10}}>Message</Text>
 </View>
 </TouchableHighlight>

 <View>
      <Iconant name="addusergroup" size={30}  style={{color:"black",padding:20}}/>
      <Text style={{paddingLeft:10}}>Matches</Text>
 </View>

 <View>
      <Iconant name="hearto" size={30}  style={{color:"black",padding:20}}/>
      <Text style={{paddingLeft:15}}>Likes</Text>
 </View>

 <View>
      <Iconfontisto name="favorite" size={30}  style={{color:"black",padding:20}}/>
      <Text style={{paddingLeft:10}}>Favorite</Text>
 </View>

    </View>

    <View style={{flexDirection:"row"}}>
      <View style={{flex:2,paddingLeft:30,paddingTop:20}}>
           <Text >Recived</Text>
      </View>

      <View style={{flex:2,paddingTop:20}}>
           <Text>Given</Text>
      </View>

      <View style={{flex:2,paddingTop:20}}>
           <Text>Declined</Text>
      </View>
    </View>

    <View style={{display:"flex",justifyContent:"center",alignItems:"center",height:"70%"}}>
      <Text style={{display:"flex"}}>No declined  likes</Text>
    </View>
   </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
