import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Roundimage from '../images/roundimg.jpg';
import { FaWindowClose } from "react-icons/fa";
import { FaFilter } from "react-icons/fa";
import { FaSignInAlt } from 'react-icons/fa';
import { FaShareAlt } from 'react-icons/fa';

import { FaMapMarkerAlt } from 'react-icons/fa';
import { FaLock } from 'react-icons/fa';
import { FaRegAddressBook } from 'react-icons/fa';
import { FaCog } from 'react-icons/fa';

import { FaQuestionCircle } from 'react-icons/fa';
import { FaGift } from 'react-icons/fa';






class Menu extends React.Component {





    render() {
        return (
            <div>
                <div className='bg-color'>
                    <div className='row'>
                        <div className='col-6'>
                            <img className='image' src={Roundimage} />
                        </div>
                        <div className='col-6'>
                            <div className='row closerow'>
                                <h3 className='center'><FaWindowClose /></h3>
                            </div>
                            <div className='row sharerow'>
                                <h3 className='center'><FaShareAlt /></h3>
                            </div>
                        </div>
                    </div>

                    <div className='row sec-two'>
                        <div className='nameedit-maindiv'>
                            <div>
                                <h4>Siva</h4>
                            </div>
                            <div>
                                <h4>Edit</h4>
                            </div>
                        </div>
                    </div>

                    <div className='row sec-three' >
                        <div className='cityname'> <FaMapMarkerAlt /><span className=' paddingicon'>Coimbatore,TamilNadu</span></div>
                    </div><br /><br></br>

                    <div className='fourth-sec'>
                        <div><FaRegAddressBook /><span className='paddingicon'>Profile</span></div>
                        <div><FaCog /><span className='paddingicon'>Setting</span></div>
                        <div><FaFilter /><span className='paddingicon'>perference</span></div>
                        <div><FaLock /><span className='paddingicon'>Privacy and saftey</span></div>
                    </div><br></br><br></br>


                    <div className='fifth-sec'>
                        <div><FaGift /><span className='paddingicon'>Refer a friend</span></div>
                        <div><FaQuestionCircle /><span className='paddingicon'>Help</span></div>
                        <div><FaSignInAlt /><span className='paddingicon'>Logout</span></div>
                    </div>


                </div>
            </div>
        )
    }
}
export default Menu